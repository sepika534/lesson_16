const todoData = [

    {
        id: 1,
        text: "Почистить зубы",
        completed: true
    },
    {
        id: 2,
        text: "Зайти на пару",
        completed: false
    },
    {
        id: 3,
        text: "Сходить в магазин",
        completed: true
    },
    {
        id: 4,
        text: "Выгулять собаку",
        completed: false
    },
    {
        id: 5,
        text: "Приготовить еду",
        completed: true
    }

]

export default todoData;
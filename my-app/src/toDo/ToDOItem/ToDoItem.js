import React from 'react'
import "./ToDoItem.css"

const ToDOItem = (props) => {
    return (
        <div className='todo-item'>
            <input type="checkbox" defaultChecked={props.completed} />
            <p className='description'>{props.description}</p>
        </div>
    )
}

export default ToDOItem;
import React from 'react';
import './App.css';
import ToDOItem from './toDo/ToDOItem/ToDoItem';
import todoData from './toDo/todoData';

function App() {
  const todoItems = todoData.map(item => {
    return (
      <ToDOItem
        key={item.id}
        description={item.text}
        completed={item.completed}
      />
    )
  })

  const inputRef = React.createRef();

  const getValue = () => {
    console.log(inputRef.current.value)
  }

  return (
    <div className="App">
      {todoItems}
      <div className='wrapper'>
        <input className='input' ref={inputRef} />
        <button onClick={getValue} className='btn'>push me</button>
      </div>
    </div>
  );
}

export default App;
